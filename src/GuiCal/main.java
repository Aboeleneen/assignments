/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GuiCal;

import eg.edu.alexu.csd.oop.calculator.Calculator;
import eg.edu.alexu.csd.oop.calculator.cs47.MyCalculator;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 *
 * @author Lenovo
 */
public class main extends Application {
    
    MyCalculator cal = new MyCalculator();
    @Override
    public void start(Stage stage) throws Exception {
        AnchorPane root = new AnchorPane();
        stage.setTitle("Cal");
        
        // input
        TextField textField = new TextField();
        textField.setLayoutX(0);
        textField.setLayoutY(0);
        textField.setMinHeight(25);
        textField.setMinWidth(200);
        textField.setEditable(false);
        root.getChildren().add(textField);
        
        // result
        TextField textField2 = new TextField();
        textField2.setLayoutX(0);
        textField2.setLayoutY(25);
        textField2.setMinHeight(25);
        textField2.setMinWidth(200);
        textField2.setEditable(false);
        root.getChildren().add(textField2);

        // Save Button
        Button buttonSave = new Button("Save");
        buttonSave.setStyle("-fx-font-weight:bold ; -fx-font-size:13px");
        buttonSave.setLayoutX(0);
        buttonSave.setLayoutY(50);
        buttonSave.setMinHeight(35);
        buttonSave.setMinWidth(50);
        buttonSave.setOnAction( e-> {cal.save();});
        root.getChildren().add(buttonSave);
        Scene scene = new Scene(root);
        
        //Load Button
        Button buttonLoad = new Button("load");
        buttonLoad.setStyle("-fx-font-weight:bold ; -fx-font-size:13px");
        buttonLoad.setLayoutX(50);
        buttonLoad.setLayoutY(50);
        buttonLoad.setMinHeight(35);
        buttonLoad.setMinWidth(50);
        buttonLoad.setOnAction( e-> {
        cal.load();
        textField.setText(cal.current());
        });
        root.getChildren().add(buttonLoad);
        
        //next Button
        Button buttonNext = new Button("Next");
        buttonNext.setStyle("-fx-font-weight:bold ; -fx-font-size:13px");
        buttonNext.setLayoutX(100);
        buttonNext.setLayoutY(50);
        buttonNext.setMinHeight(35);
        buttonNext.setMinWidth(50);
        buttonNext.setOnAction( e-> {
       
           String next = cal.next();
            if(next != null)  textField.setText(next);
            else textField.setText("");
        
        });
        root.getChildren().add(buttonNext);
        
        //Prev Button
        Button buttonPrev = new Button("Prev");
        buttonPrev.setStyle("-fx-font-weight:bold ; -fx-font-size:13px");
        buttonPrev.setLayoutX(150);
        buttonPrev.setLayoutY(50);
        buttonPrev.setMinHeight(35);
        buttonPrev.setMinWidth(50);
        buttonPrev.setOnAction( e-> {
            String prev = cal.prev();
            if(prev != null)  textField.setText(prev);
            else textField.setText("");
      
        });
        root.getChildren().add(buttonPrev);
        
        // 7
        Button button7 = new Button("7");
        button7.setStyle("-fx-font-weight:bold ; -fx-font-size:13px");
        button7.setLayoutX(0);
        button7.setLayoutY(85);
        button7.setMinHeight(35);
        button7.setMinWidth(50);
        button7.setOnAction( e-> {textField.setText(textField.getText()+"7");});
        root.getChildren().add(button7);
        
        // 8
        Button button8 = new Button("8");
        button8.setStyle("-fx-font-weight:bold ; -fx-font-size:13px");
        button8.setLayoutX(50);
        button8.setLayoutY(85);
        button8.setMinHeight(35);
        button8.setMinWidth(50);
        button8.setOnAction( e-> {textField.setText(textField.getText()+"8");});
        root.getChildren().add(button8);
        
            // 9
        Button button9 = new Button("9");
        button9.setStyle("-fx-font-weight:bold ; -fx-font-size:13px");
        button9.setLayoutX(100);
        button9.setLayoutY(85);
        button9.setMinHeight(35);
        button9.setMinWidth(50);
        button9.setOnAction( e-> {textField.setText(textField.getText()+"9");});
        root.getChildren().add(button9);
        
        // *
        Button buttonX = new Button("*");
        buttonX.setStyle("-fx-font-weight:bold ; -fx-font-size:13px");
        buttonX.setLayoutX(150);
        buttonX.setLayoutY(85);
        buttonX.setMinHeight(35);
        buttonX.setMinWidth(50);
        buttonX.setOnAction( e-> {textField.setText(textField.getText()+"*");});
        root.getChildren().add(buttonX);
        
        // 4
                Button button4 = new Button("4");
        button4.setStyle("-fx-font-weight:bold ; -fx-font-size:13px");
        button4.setLayoutX(0);
        button4.setLayoutY(85+35);
        button4.setMinHeight(35);
        button4.setMinWidth(50);
        button4.setOnAction( e-> {textField.setText(textField.getText()+"4");});
        root.getChildren().add(button4);
        
             // 5
                Button button5 = new Button("5");
        button5.setStyle("-fx-font-weight:bold ; -fx-font-size:13px");
        button5.setLayoutX(50);
        button5.setLayoutY(85+35);
        button5.setMinHeight(35);
        button5.setMinWidth(50);
        button5.setOnAction( e-> {textField.setText(textField.getText()+"5");});
        root.getChildren().add(button5);
        
                     // 6
        Button button6 = new Button("6");
        button6.setStyle("-fx-font-weight:bold ; -fx-font-size:13px");
        button6.setLayoutX(100);
        button6.setLayoutY(85+35);
        button6.setMinHeight(35);
        button6.setMinWidth(50);
        button6.setOnAction( e-> {textField.setText(textField.getText()+"6");});
        root.getChildren().add(button6);
        
        // div
                Button buttonDiv = new Button("/");
        buttonDiv.setStyle("-fx-font-weight:bold ; -fx-font-size:13px");
        buttonDiv.setLayoutX(150);
        buttonDiv.setLayoutY(85+35);
        buttonDiv.setMinHeight(35);
        buttonDiv.setMinWidth(50);
        buttonDiv.setOnAction( e-> {textField.setText(textField.getText()+"/");});
        root.getChildren().add(buttonDiv);
        
        // 1
        
                Button button1 = new Button("1");
        button1.setStyle("-fx-font-weight:bold ; -fx-font-size:13px");
        button1.setLayoutX(0);
        button1.setLayoutY(85+35+35);
        button1.setMinHeight(35);
        button1.setMinWidth(50);
        button1.setOnAction( e-> {textField.setText(textField.getText()+"1");});
        root.getChildren().add(button1);
        
                // 2
        Button button2 = new Button("2");
        button2.setStyle("-fx-font-weight:bold ; -fx-font-size:13px");
        button2.setLayoutX(50);
        button2.setLayoutY(85+35+35);
        button2.setMinHeight(35);
        button2.setMinWidth(50);
        button2.setOnAction( e-> {textField.setText(textField.getText()+"2");});
        root.getChildren().add(button2);
        
                        // 3
        Button button3 = new Button("3");
        button3.setStyle("-fx-font-weight:bold ; -fx-font-size:13px");
        button3.setLayoutX(100);
        button3.setLayoutY(85+35+35);
        button3.setMinHeight(35);
        button3.setMinWidth(50);
        button3.setOnAction( e-> {textField.setText(textField.getText()+"3");});
        root.getChildren().add(button3);
        
                                // sub
        Button buttonSub = new Button("-");
        buttonSub.setStyle("-fx-font-weight:bold ; -fx-font-size:13px");
        buttonSub.setLayoutX(150);
        buttonSub.setLayoutY(85+35+35);
        buttonSub.setMinHeight(35);
        buttonSub.setMinWidth(50);
        buttonSub.setOnAction( e-> {textField.setText(textField.getText()+"-");});
        root.getChildren().add(buttonSub);
        
                                // Zero
        Button buttonZero = new Button("0");
        buttonZero.setStyle("-fx-font-weight:bold ; -fx-font-size:13px");
        buttonZero.setLayoutX(0);
        buttonZero.setLayoutY(85+35+35+35);
        buttonZero.setMinHeight(35);
        buttonZero.setMinWidth(50);
        buttonZero.setOnAction( e-> {textField.setText(textField.getText()+"0");});
        root.getChildren().add(buttonZero);
        
                                      // Point
        Button buttonPoint = new Button(".");
        buttonPoint.setStyle("-fx-font-weight:bold ; -fx-font-size:13px");
        buttonPoint.setLayoutX(50);
        buttonPoint.setLayoutY(85+35+35+35);
        buttonPoint.setMinHeight(35);
        buttonPoint.setMinWidth(50);
        buttonPoint.setOnAction( e-> {textField.setText(textField.getText()+".");});
        root.getChildren().add(buttonPoint);
        
           // equal
           
           Button buttonEqual = new Button("=");
        buttonEqual.setStyle("-fx-font-weight:bold ; -fx-font-size:13px");
        buttonEqual.setLayoutX(100);
        buttonEqual.setLayoutY(85+35+35+35);
        buttonEqual.setMinHeight(35);
        buttonEqual.setMinWidth(50);
        buttonEqual.setOnAction( e-> {
        
          cal.input(textField.getText());
          textField2.setText(cal.getResult());
          textField.setText("");
        
        });
        root.getChildren().add(buttonEqual);
        
        //  +
                Button buttonAdd = new Button("+");
        buttonAdd.setStyle("-fx-font-weight:bold ; -fx-font-size:13px");
        buttonAdd.setLayoutX(150);
        buttonAdd.setLayoutY(85+35+35+35);
        buttonAdd.setMinHeight(35);
        buttonAdd.setMinWidth(50);
        buttonAdd.setOnAction( e-> {textField.setText(textField.getText()+"+");});
        root.getChildren().add(buttonAdd);
        
        
        
       
        
        
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
