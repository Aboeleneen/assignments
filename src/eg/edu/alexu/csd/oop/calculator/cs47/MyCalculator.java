
package eg.edu.alexu.csd.oop.calculator.cs47;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Deque;
import java.util.LinkedList;

import eg.edu.alexu.csd.oop.calculator.Calculator;

public class MyCalculator implements Calculator{
	
	 
	double firstNum,secondNum;
	char operator=' ';
	String currentInput;
	int currentOper = 0;
	Deque<Operation> operations = new LinkedList<Operation>();
	

	@Override
	public void input(String s) {
		// TODO Auto-generated method stub
		
		
		
		
		handle(s);
		addToHistory(currentInput);
		currentOper = operations.size();
		
	}

	@Override
	public String getResult(){
		// TODO Auto-generated method stub
		double output;
		String res="";
		handle(current());
		if(operator == '+'){
			output=firstNum + secondNum;
			res=String.valueOf(output);
			 
			return res;	
		}
		else if(operator == '-'){
			output=firstNum - secondNum;
			res=String.valueOf(output);
		
			return res;	
		}
		else if(operator == '*'){
			output=firstNum * secondNum;
			res=String.valueOf(output);
			
			return res;	
		}
		else if(operator == '/'){
			output=firstNum / secondNum;
			res=String.valueOf(output);
			
			return res;	
		}
		return null;
	}

	@Override
	public String current() {
		// TODO Auto-generated method stub
		
			int i=1;
			
			for(Operation object: operations){
				if(i == currentOper){
					        
							 return(object.getOper());
							
				}
				i++;
			}
		
		return null;
	}

	@Override
	public String prev() {
		// TODO Auto-generated method stub
		if (currentOper-1 >= 1){
			currentOper--;
			int i=1;
			for(Operation object: operations){
				if(i == currentOper)
					       return(object.getOper());
				i++;
			}
		}
		return null;
	}

	@Override
	public String next() {
		// TODO Auto-generated method stub
		if (currentOper+1 <= operations.size() ){
			currentOper++;
			int i=1;
			for(Operation object: operations){
				if(i == currentOper)
							return(object.getOper());
				i++;
			}
		}
		return null;
	}

	@Override
	public void save() {
		// TODO Auto-generated method stub
		try {
			FileWriter file = new FileWriter("history.txt");
			for(Operation object: operations){
				
						file.write(object.getOper() + "#");
				
			}
			file.write(currentOper + "#");
			file.close();
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void load() {
		// TODO Auto-generated method stub
		try {
			FileReader file = new FileReader("history.txt");
			operations.clear();
			String oper = "";
			int c ;
			while((c=file.read()) != -1){
				if( (char) c  != '#'){
					oper+= (char) c;
				}
				else{
					if(oper.length() != 1) input(oper);
					else {
						currentOper = Integer.parseInt(oper);
					}
					oper="";
				}
				
			}
			for(Operation object: operations){
				
				
		
	         }
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public void addToHistory(String oper ){
		if (operations.size() >= 5){
			operations.removeFirst();
			operations.add(new Operation(oper,"0"));
		}
		else {
			operations.add(new Operation(oper,"0"));
		}
	}
	
	public void handle(String s){
		currentInput = s;
		operator=' ';
		String first="",second="";
		Boolean flag=false;
		for(int i=0;i<s.length();i++){
			if(s.charAt(i) == '+' || ((s.charAt(i) == '-' && ! flag)&& first !="")  || s.charAt(i) =='*' || s.charAt(i) == '/' ){
				flag=true;
				operator=s.charAt(i);
				continue;
			}
			if( ! flag){
				first+=s.charAt(i);
			}
			else {
				second+=s.charAt(i);
			}
		}
		if(first.charAt(0) == '-'){
			first = first.substring(1,first.length());
			firstNum  = -1*Double.parseDouble(first);
		}
		else{
			firstNum  = Double.parseDouble(first);
		}
		if(second.charAt(0) == '-'){
			second = second.substring(1,second.length());
			secondNum  = -1*Double.parseDouble(second);
		}
		else{
			secondNum  = Double.parseDouble(second);
		}
	}
	

}
