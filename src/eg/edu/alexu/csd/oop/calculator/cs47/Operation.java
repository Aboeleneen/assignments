package eg.edu.alexu.csd.oop.calculator.cs47;

class Operation {
    private String oper;
    private String result;
    private String status;
    
    public Operation(String oper,String result,String status){
    	this.oper=oper;
    	this.result=result;
    	this.status = status;
    }
    public Operation(String oper,String status){
    	this.oper=oper;
    	this.status = status;
    }



	public String getOper() {
		return oper;
	}

	public void setOper(String oper) {
		this.oper = oper;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
    
    
    
}
